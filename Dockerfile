FROM python:3

RUN apt-get update            && \
    apt-get -y install vim

RUN pip install --upgrade pip && \
    pip --version             && \
    pip install boto3         && \
    pip install awscli        && \
    pip install moto          && \
    pip install pytest        && \
    pip list

CMD bash
